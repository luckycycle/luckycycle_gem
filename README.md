# Luckycycle

The Luckycycle gem allows the creation of pokes on the Luckycycle platform. You need an operation_id (or a store_id) and an API key.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'luckycycle'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install luckycycle

## Usage

Initialize gem with your operation id and your api_key
```ruby
lucky = Luckycycle.new(operation_id, api_key)
```
or via your store_id
```ruby
lucky = Luckycycle.new(store_id, api_key, 'store')
```

add values of cart
```ruby
lucky.add_values({item_value: '20.2', segment: 'A', language: 'en', user_uid: 120512, item_uid: 150252, email: 'john.doe@gmail.com', firstname: 'john', lastname: 'doe', locale: 'fr', item_currency: 'EUR'})
```
item_value: value of sum of cart
item_uid: id of order (string)
user_uid: id of user (string)
language: fr, en, nl, de, ... (string)
segment: AB testing (string)
item_currency; EUR, GBP, USD, ... (string) (https://en.wikipedia.org/wiki/ISO_4217)

for hybrid mode add content of each items of cart
```ruby
lucky.add_cart_item({'price'=>'30' , 'quantity'=>2 , 'product_id'=>'155'})
lucky.add_cart_item({'price'=>'121.5' , 'quantity'=>4 , 'product_id'=>'156'})
```

make the call
```ruby
lucky.poke
```

return data in json
```json
{"can_play" => true,
 "message" => "ok",
 "code"=> 800,
 "item_uid" => "xxx",
 "user_uid" => "xxx",
 "computed_hash" => "xxx",
 "poke_counter" => 1,
 "value_played" => "xxx",
 "cluster_id" => nil,
 "user_played" => "xxx",
 "banner_url" => nil,
 "html_data" => "<a class='js_after_poke' href='https://www.luckycycle.com/play/xxx'>Click here to play the Luckycycle game</a>"}
```

 if code is 800 and message ok

you can call this method to get the html code to put in the confirmation page
```ruby
lucky.html()
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release` to create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

1. Fork it ( https://github.com/[my-github-username]/luckycycle/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request