require "luckycycle/version"
require 'rest-client'

class Luckycycle
	def initialize(id, api_key, type = 'operation')
		types = %{operation store}
		if types.include?(type)
		  @api_key = api_key
			@id = id
			@type = type
			@cart_items  = []
			@response = ''
			@data = {}
		else
			return false
		end
	end

	def add_values(options = {})
		options.each do |key, value|
		@data[key] = value
		end
	end

	def add_cart_item(options = {})
		@cart_items.push(options)
		@data['cart'] = @cart_items
	end

	def poke
		url = "https://www.luckycycle.com/api/v1/#{@type}/#{@id}/poke"

		data = @data.to_json
		response = RestClient::Request.execute(
			:method => :post,
			:url => url,
			:payload => data,
			:timeout => 9000000,
			:headers => {'X-LuckyApiKey' => @api_key, 'Content-Type' => 'application/json'}
		)
		@response = JSON.parse(response)
		return @response
	end

	def html
 		return @response['html_data']
	end
end
