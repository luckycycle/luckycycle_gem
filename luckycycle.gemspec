# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'luckycycle/version'

Gem::Specification.new do |spec|
  spec.name          = "luckycycle"
  spec.version       = Luckycycle::VERSION
  spec.authors       = ["guillaume"]
  spec.email         = ["gsartenaer@luckycycle.com"]

  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com' to prevent pushes to rubygems.org, or delete to allow pushes to any server."
  end

  spec.summary       = %q{gem to create poke on luckycycle}
  spec.description   = %q{gem to create poke on luckycycle}
  #spec.homepage      = "TODO: Put your gem's website or public repo URL here."
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.8.pre"
  spec.add_development_dependency "rake", "~> 10.0"
end
